﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace GameOfThrones
{
    // https://app.quicktype.io/#r=json2csharp
    // <auto-generated />
    //
    // To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
    //
    //    using QuickType;
    //
    //    var Heroes = Hero.FromJson(jsonString);
    public class Pagerank
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("rank")]
        public long Rank { get; set; }
    }

    public partial class Hero
        {
            [JsonProperty("titles")]
            public List<string> Titles { get; set; }

            [JsonProperty("spouse")]
            public List<string> Spouse { get; set; }

            [JsonProperty("children")]
            public List<string> Children { get; set; }

            [JsonProperty("allegiance")]
            public List<string> Allegiance { get; set; }

            [JsonProperty("books")]
            public List<String> Books { get; set; }

            [JsonProperty("plod")]
            public long Plod { get; set; }

            [JsonProperty("longevity")]
            public List<object> Longevity { get; set; }

            [JsonProperty("plodB")]
            public double PlodB { get; set; }

            [JsonProperty("plodC")]
            public long PlodC { get; set; }

            [JsonProperty("longevityB")]
            public List<double> LongevityB { get; set; }

            [JsonProperty("longevityC")]
            public List<object> LongevityC { get; set; }

            [JsonProperty("_id")]
            public string Id { get; set; }

            [JsonProperty("name")]
            public string Name { get; set; }

            [JsonProperty("slug")]
            public string Slug { get; set; }

            [JsonProperty("gender")]
            public String Gender { get; set; }

            [JsonProperty("culture", NullValueHandling = NullValueHandling.Ignore)]
            public string Culture { get; set; }

            [JsonProperty("house", NullValueHandling = NullValueHandling.Ignore)]
            public string House { get; set; }

            [JsonProperty("alive")]
            public bool Alive { get; set; }

            [JsonProperty("createdAt")]
            public DateTimeOffset CreatedAt { get; set; }

            [JsonProperty("updatedAt")]
            public DateTimeOffset UpdatedAt { get; set; }

            [JsonProperty("__v")]
            public long V { get; set; }

            [JsonProperty("pagerank")]
            public Pagerank Pagerank { get; set; }

            [JsonProperty("id")]
            public string HeroId { get; set; }

            [JsonProperty("image", NullValueHandling = NullValueHandling.Ignore)]
            public String Image { get; set; }

            [JsonProperty("birth", NullValueHandling = NullValueHandling.Ignore)]
            public long? Birth { get; set; }

            [JsonProperty("placeOfDeath", NullValueHandling = NullValueHandling.Ignore)]
            public string PlaceOfDeath { get; set; }

            [JsonProperty("death", NullValueHandling = NullValueHandling.Ignore)]
            public long? Death { get; set; }

            [JsonProperty("placeOfBirth", NullValueHandling = NullValueHandling.Ignore)]
            public string PlaceOfBirth { get; set; }

            [JsonProperty("longevityStartB", NullValueHandling = NullValueHandling.Ignore)]
            public long? LongevityStartB { get; set; }

            [JsonProperty("father", NullValueHandling = NullValueHandling.Ignore)]
            public string Father { get; set; }

            [JsonProperty("mother", NullValueHandling = NullValueHandling.Ignore)]
            public string Mother { get; set; }

            [JsonProperty("heir", NullValueHandling = NullValueHandling.Ignore)]
            public string Heir { get; set; }

    }

    public partial class Hero
    {
        public static List<Hero> FromJson(string json) => JsonConvert.DeserializeObject<List<Hero>>(json);
    }

    public static class Serialize
    {
        public static string ToJson(this List<Hero> self) => JsonConvert.SerializeObject(self);
    }

}
