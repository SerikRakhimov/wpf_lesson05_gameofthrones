﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CefSharp;
using CefSharp.Wpf;
using System.Xml.Serialization;
using Newtonsoft.Json;
using System.IO;
using System.Net;

namespace GameOfThrones
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Hero> heroesAll;
        List<Hero> heroesFilter;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //          PrBar.Visibility = Visibility.Visible;
            WebRequest req = WebRequest.Create(@"https://api.got.show/api/book/characters");

            string jsonText = "";

            WebResponse response = req.GetResponse();
            using (Stream s = response.GetResponseStream()) //Пишем в поток.
            {
                using (StreamReader r = new StreamReader(s)) //Читаем из потока.
                {
                    jsonText = r.ReadToEnd();
                }
            }
            response.Close(); //Закрываем поток

            heroesAll = Hero.FromJson(jsonText).ToList();
            heroesFilter = heroesAll;

            heroesFilter.Sort(delegate (Hero hero1, Hero hero2)
            { return hero1.Name.CompareTo(hero2.Name); });

            ListData.Items.Clear();
            foreach (var t in heroesFilter)
            {
                var textBlockWork = new TextBlock();
                textBlockWork.Text = t.Name;
                ListData.Items.Add(textBlockWork);
            }
        }


        private void TextBoxFind_TextChanged(object sender, TextChangedEventArgs e)
        {
            heroesFilter = heroesAll.Where(song => song.Name.Contains(TextBoxFind.Text.Trim())).ToList();

            heroesFilter.Sort(delegate (Hero hero1, Hero hero2)
            { return hero1.Name.CompareTo(hero2.Name); });

            ListData.Items.Clear();
            foreach (var t in heroesFilter)
            {
                var textBlockWork = new TextBlock();
                textBlockWork.Text = t.Name;
                ListData.Items.Add(textBlockWork);
            }
        }


        private void ListData_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            if (ListData.SelectedIndex >= 0)
            {
                WindowAbout wind = new WindowAbout();

                BitmapImage bmp = new BitmapImage();
                bmp.BeginInit();
                if (heroesFilter[ListData.SelectedIndex].Image != null)
                {
                    bmp.UriSource = new Uri(heroesFilter[ListData.SelectedIndex].Image.ToString(), UriKind.RelativeOrAbsolute);
                }
                else
                { 
                    bmp.UriSource = new Uri("Image/NoPhoto.png", UriKind.Relative);
                }
                bmp.EndInit();
                wind.Image.Source = bmp;
                     
                wind.Name.Text = heroesFilter[ListData.SelectedIndex].Name;
                wind.Culture.Text = heroesFilter[ListData.SelectedIndex].Culture != null ? heroesFilter[ListData.SelectedIndex].Culture.ToString() : "не указана";
                wind.House.Text = heroesFilter[ListData.SelectedIndex].House != null ? heroesFilter[ListData.SelectedIndex].House.ToString() : "не указан";
                wind.Gender.Text = heroesFilter[ListData.SelectedIndex].Gender != null ? heroesFilter[ListData.SelectedIndex].Gender.ToString() : "не указан";
                wind.ShowDialog();

            }
        }
    }
}
